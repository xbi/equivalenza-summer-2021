'use strict';

import autoprefixer from 'autoprefixer';
import babelify from 'babelify';
import browserify from 'browserify';
import browserSync from 'browser-sync';
import buffer from 'vinyl-buffer';
import cssnano from 'cssnano';
import dartSass from 'sass';
import del from 'del';
import gulp from 'gulp';
import plugins from 'gulp-load-plugins';
import postcssCombineMediaQuery from 'postcss-combine-media-query';
import postcssFlexbugsFixes from 'postcss-flexbugs-fixes';
import source from 'vinyl-source-stream';
import stylish from 'jshint-stylish';
import yargs from 'yargs';

const $ = plugins();
const PRODUCTION = !!(yargs.argv.production);
const FILENAME = yargs.argv.filename;

const libs = {
  // 'swiper': 'dist/**/swiper.min.{js,css}' // Touch slider
};

// Specify Sass compiler
const sass = $.sass(dartSass);

//
// Clean dist folder before build
// ----------------------------------------------------------------------------
gulp.task('clean', () => {
  return del(['dist/**/*', '!dist/ficha-producto', '!dist/ficha-producto/**/*', '!dist/ficha-producto-configurable', '!dist/ficha-producto-configurable/**/*']);
});

//
// Copy fonts to dist
// ----------------------------------------------------------------------------
gulp.task('fonts', () => {
  return gulp.src([
    './src/fonts/PlaylistScript/*.{woff,woff2}'
  ]).pipe(gulp.dest('./dist/assets/fonts'));
});

//
// Copy 3rd party libs to dist
// ----------------------------------------------------------------------------
gulp.task('libs', (done) => {

  if (Object.entries(libs).length > 0) {
    Object.entries(libs).forEach(([pkg, pathToFiles]) => {
      return gulp.src([`./node_modules/${pkg}/${pathToFiles}`, `!./node_modules/${pkg}/${pathToFiles}.map`]) // Copy all files except .map
        .pipe($.sourcemaps.init({ loadMaps: true })) // Remove link to maps from JS/CSS files
        .pipe(gulp.dest(`./dist/assets/libs/${pkg}`));
    });
  }

  done();
});

//
// Copy static assets and mantain folder structure
// ----------------------------------------------------------------------------
gulp.task('static', () => {
  return gulp.src('./src/static/**', { base: './src/static/' })
  .pipe(gulp.dest('./dist/'));
});

//
// SVG sprite generator
// ----------------------------------------------------------------------------
gulp.task('sprite', () => {

  const SVGs = gulp.src(['src/icomoon/symbol-defs.svg'], { allowEmpty: true });

  const fileContents = (filePath, file) => file.contents.toString();

  return gulp
    .src('./src/sprite-svg.twig')
    .pipe($.inject(SVGs, {
      transform: fileContents
    }))
    .pipe(gulp.dest('./src/templates'))
    .pipe(gulp.dest('./src/templates/common'));
});

//
// Copy images to dist
// ----------------------------------------------------------------------------
gulp.task('images', () => {
  return gulp.src('./src/images/**/*.{gif,png,jpeg,jpg,svg}')
    // .pipe($.if(file => { return (PRODUCTION && file.extname !== '.svg') }, $.smushit({ verbose: true })))
    .pipe(gulp.dest('./dist/assets/images'));
});

//
// Compile Twig templates
// ----------------------------------------------------------------------------
gulp.task('html', () => {
  return gulp.src('./src/pages/**/*.twig')
    .pipe($.twig({
      base: './src/templates/'
    }))
    .pipe($.prettier({ parser: 'html' }))
    .pipe(gulp.dest('./dist/'));
});

//
// Build CSS
// ----------------------------------------------------------------------------
gulp.task('css', () => {
  // Default PostCSS plugins
  const PostCssPlugins = [
    autoprefixer(), // Add vendor prefixes for the targeted browsers
    postcssFlexbugsFixes(), // PostCSS plugin that tries to fix all of flexbugs issues
  ];

  // PostCSS plugins for production builds
  if (PRODUCTION) {
    PostCssPlugins.push(postcssCombineMediaQuery()); // Pack all equal CSS media query rules into one
    PostCssPlugins.push(cssnano()); // Minify CSS for production
  }

  return gulp.src('./src/scss/**/*.scss')
    .pipe($.if(!PRODUCTION, $.sourcemaps.init()))
    .pipe(sass({
      outputStyle: $.if(!PRODUCTION, 'expanded', 'compressed'),
    }).on('error', sass.logError))
    .pipe($.postcss(PostCssPlugins))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest('./dist/assets/css/'))
    .pipe(browserSync.stream());
});

//
// Build JavaScript bundle
// ----------------------------------------------------------------------------
gulp.task('bundle', () => {
  return browserify({
    entries: ['./src/scripts/main.js']
  })
    .transform(babelify.configure({
      presets: ['@babel/preset-env']
    }))
    .bundle()
    .pipe(source('scripts.js'))
    .pipe(buffer())
    .pipe($.if(!PRODUCTION, $.sourcemaps.init({
      loadMaps: true
    })))
    .pipe($.if(PRODUCTION, $.uglify()))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write('./')))
    .pipe(gulp.dest('./dist/assets/js/'))
});

//
// JShint JavaScript Bundle
// ---------------------------------------------------------------------------
gulp.task('hintScripts', () => {
  return gulp.src('./src/scripts/**/*.js')
    .pipe($.jshint({
      esversion: 6,
      laxbreak: true
    }))
    .pipe($.jshint.reporter(stylish));
});

//
// Prettify code
// ---------------------------------------------------------------------------
gulp.task('prettifyJS', () => {
  return gulp.src('./src/scripts/**/*.js')
    .pipe($.prettier({
      singleQuote: true,
      laxbreak: true
    }))
    .pipe(gulp.dest(file => file.base));
});

gulp.task('prettifyCSS', () => {
  return gulp.src('./src/styles/**/*.css')
    .pipe($.prettier())
    .pipe(gulp.dest(file => file.base));
});

//
// Watch for file changes
// ----------------------------------------------------------------------------
gulp.task('watch', (done) => {
  gulp.watch('./src/scss/**/*.scss')
    .on('change', gulp.series('css'));
  gulp.watch('./src/scripts/**/*.js')
    .on('change', gulp.series('hintScripts', 'bundle', browserSync.reload));
  gulp.watch(['./src/pages/**/*.twig', './src/templates/**/*.twig'])
    .on('change', gulp.series('html', browserSync.reload));
  gulp.watch('./src/images/**/*.{gif,png,jpeg,jpg,svg}')
    .on('change', gulp.series('images', browserSync.reload));
  gulp.watch('./src/static/**', gulp.series('static'));
  done();
});

//
// Start a server with LiveReload to preview the site
// ----------------------------------------------------------------------------
gulp.task('server', gulp.parallel('watch', () => {
  browserSync.init({
    open: false,
    server: {
      baseDir: './dist/'
    }
  });
}));

//
// Zip dist folder
// ----------------------------------------------------------------------------
gulp.task('zip', (done) => {
  if (!FILENAME) {
    console.log($.color('You must specify a name for the zip file, p.e: gulp zip --filename EAE-163.zip', 'RED'));
    done();
  } else {
    console.log($.color(`Packed ${FILENAME}!`, 'GREEN'));
    return gulp.src('./dist/**')
      .pipe($.zip(`${FILENAME}`))
      .pipe(gulp.dest('./'));
  }
});

//
// Build theme assets
// ----------------------------------------------------------------------------
gulp.task('build', gulp.series('clean', gulp.parallel('images', 'libs', 'css', 'bundle', 'sprite', 'static'), 'html'));

//
// Build theme, run the server, and watch for file changes
// ----------------------------------------------------------------------------
gulp.task('default', gulp.series('build', 'server'));

//
// Run JShint on JS modules and prettify theme's Sass & JS source code
// ----------------------------------------------------------------------------
gulp.task('tidy', gulp.series('hintScripts', 'prettifyJS', 'prettifyCSS'));
