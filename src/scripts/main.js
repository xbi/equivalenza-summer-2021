global.$ = window.jQuery;

import landings from './modules/landings';
import forms from './modules/forms';

$(() => {
  const DEBUG = true; // Set to false to disable console methods output in all modules

  if (!DEBUG) {
    if (!window.console) window.console = {};
    const methods = ['log', 'debug', 'warn', 'info'];
    methods.forEach(method => {
      console[method] = () => {};
    });
  }

  console.log('🏎 Initializing website…');

  landings();
  forms();

  console.log('🏁 Website initialized!');
});  