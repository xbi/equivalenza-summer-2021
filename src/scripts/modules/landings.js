export default () => {
    const IS_LANDING = $('.eq-landing');

    if (IS_LANDING) {
        // Init product slider
        const $productItems = $('.product.product-item').not('.init');

        if ($productItems.length) {
            $productItems.each(function () {
                const $this = $(this);
                $this.addClass('init');
                const $link = $this.find('.product-item-link');
                const url = $link.attr('href');
                const $button = $(`<a href="${url}" class="btn btn-primary">${i18n('label_buy')}</a>`);
                $button.appendTo($this);
            });
        }

        console.log('✅ Landing initialized…');
    }
};
