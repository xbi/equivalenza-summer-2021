export default () => {
    const $franquiciasForm = $('#franquicias-form');

    if ($franquiciasForm.length) {
        $franquiciasForm.validate();
        console.log('✅ Franchises form initialized…');
    }
};